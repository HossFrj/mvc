﻿using Main.Models;
using NHibernate;

namespace Main.Repository
{
    public class AccountRepository
    {
        private readonly NHibernate.ISession _session;
        private ITransaction? _transaction;

        public AccountRepository(NHibernate.ISession session)
        {
            _session = session;
        }

        public IQueryable<Account> Accounts => _session.Query<Account>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(Account entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(Account entity)
        {
            await _session.DeleteAsync(entity);
        }
    }
}
