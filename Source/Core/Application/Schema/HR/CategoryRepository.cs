﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Schema.HR
{
    public class CategoryRepository
    {
        private readonly ISession _session;
        private ITransaction _transaction;

        public CategoryRepository(ISession session)
        {
            _session = session;
        }

        public IQueryable<Category> Categories => _session.Query<Category>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(Category category)
        {
            await _session.SaveOrUpdateAsync(category);
        }

        public async Task Delete(Category category)
        {
            await _session.DeleteAsync(category);
        }
    }
}
