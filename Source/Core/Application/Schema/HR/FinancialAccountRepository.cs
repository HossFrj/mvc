﻿using Domain.Concrete.Schema.HR;
using NHibernate;
namespace Application.Schema.HR
{



    public class FinancialAccountRepository
    {
        private readonly NHibernate.ISession _session;
        private ITransaction? _transaction;

        public FinancialAccountRepository(NHibernate.ISession session)
        {
            _session = session;
        }

        public IQueryable<FinancialAccount> FinancialAccounts => _session.Query<FinancialAccount>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(FinancialAccount entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(FinancialAccount entity)
        {
            await _session.DeleteAsync(entity);
        }
    }
}


