﻿using Main.Models;
using NHibernate;

namespace Main.Repository
{
    public class GenderReopsitory
    {
        private readonly NHibernate.ISession _session;
        private ITransaction? _transaction;

        public GenderReopsitory(NHibernate.ISession session)
        {
            _session = session;
        }


        public IQueryable<Gender> Genders => _session.Query<Gender>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(Gender entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(Gender entity)
        {
            await _session.DeleteAsync(entity);
        }
    }
}
