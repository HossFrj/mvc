﻿using Domain.Concrete.Schema.HR;
using Main.Models;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Schema.HR
{
    public class RoleRepository
    {
        private readonly NHibernate.ISession _session;
        private ITransaction? _transaction;

        public RoleRepository(NHibernate.ISession session)
        {
            _session = session;
        }

        public IQueryable<MyRole> Roles => _session.Query<MyRole>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(MyRole entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(MyRole entity)
        {
            await _session.DeleteAsync(entity);
        }
    }
}

