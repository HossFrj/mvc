﻿using Domain.Concrete.Schema.HR;
using NHibernate;


namespace Application.Schema.HR
{
    public class TransactionRepository
    {

        private readonly NHibernate.ISession _session;
        private ITransaction? _transaction;

        public TransactionRepository(NHibernate.ISession session)
        {
            _session = session;
        }

        //public IQueryable<Transaction> Transactions => _session.Query<Transaction>();
        public IQueryable<myTransaction> Transactions => _session.Query<myTransaction>();
        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(myTransaction entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(myTransaction entity)
        {
            await _session.DeleteAsync(entity);
        }

       
    }
}
