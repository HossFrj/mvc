﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Schema.HR
{
    public class TransactionTypeCategoryRepository
    {
        private readonly ISession _session;
        private ITransaction _transaction;

        public TransactionTypeCategoryRepository(ISession session)
        {
            _session = session;
        }

        public IQueryable<TransactionType_Category> TransactionTypeCategories => _session.Query<TransactionType_Category>();

        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(TransactionType_Category transactionTypeCategory)
        {
            await _session.SaveOrUpdateAsync(transactionTypeCategory);
        }

        public async Task Delete(TransactionType_Category transactionTypeCategory)
        {
            await _session.DeleteAsync(transactionTypeCategory);
        }
    }
}
