﻿using Domain.Concrete.Schema.HR;

namespace Main.Models
{
    public class  Account
    {
        public virtual int ID { get; set; }
        public virtual int PersonID { get; set; }
        public virtual int RoleID { get; set; }
        public virtual int PasswordID { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime CreateDateTime { get; set; }
        public virtual Person Person { get; set; }

        public virtual MyRole Role { get; set; }

        public virtual Password Password { get; set; }

    }
}
