﻿namespace Domain.Concrete.Schema.HR
{
    public class Category
    {
        


        public virtual int ID { get; set; }
        public virtual Category ParentCategory { get; set; }

         
        public virtual int? ParentID { get; set; }

        public virtual string? TitleEn { get; set; }

        public virtual string? TitleFa { get; set; }
        public virtual string? Description { get; set; }
        //public virtual Category ParentCategory { get; set; }

    }
}
