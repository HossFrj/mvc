﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Domain.Concrete.Schema.HR
{
    public class FinancialAccount
    {
        public virtual int ID { get; set; }
        
        public virtual string AccountID { get; set; }
        public virtual string AccountNumber { get; set; }
        [Unicode]
        [CreditCard]
        public virtual string CardNumber { get; set; }
       
        public virtual DateTime CardExpireDate { get; set; }

        public virtual string BankName { get; set; }
        public virtual decimal Balance { get; set; }
        //public virtual byte[] Icon { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime CreateDateTime { get; set; }

        public FinancialAccount()
        {
            CreateDateTime = DateTime.Now;
        }




        // public virtual IdentityUser Account { get; set; }
    }
}
