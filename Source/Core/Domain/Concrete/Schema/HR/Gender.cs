﻿namespace Main.Models
{
    public class Gender
    {
        public virtual byte Code { get; set; }
        public virtual string? TitleEn { get; set; }
        public virtual string? TitleFa { get; set; }
    }
}
