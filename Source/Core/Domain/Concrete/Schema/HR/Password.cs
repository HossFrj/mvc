﻿using Main.Models;

namespace Domain.Concrete.Schema.HR
{
    public class Password
    {
        public virtual int ID { get; set; }
        public virtual int AccountID { get; set; }
        public virtual string Pass { get; set; }
        public virtual DateTime CreateDateTime { get; set; }

        public virtual Account Account { get; set; }
    }
}
