﻿namespace Main.Models
{
    public class Person
    {
        public virtual int ID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual byte GenderCode { get; set; }
        public virtual string NationalID { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual int Num { get; set; }

        public virtual Gender Gender { get; set; }

    }
}
