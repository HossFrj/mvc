﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Concrete.Schema.HR;

public class RolesModels
{
}

public class CreateRoleViewModel
{

    public string Name { get; set; }
}
public class EditRoleViewModel
{
    public string Id { get; set; }
    [Required]
    public string Name { get; set; }
}