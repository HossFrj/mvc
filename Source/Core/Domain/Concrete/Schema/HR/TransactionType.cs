﻿namespace Domain.Concrete.Schema.HR
{
    public class TransactionType
    {
        public virtual byte Code { get; set; }
        public virtual string? TitleEn { get; set; }
        public virtual string? TitleFa { get; set; }
    }
}
