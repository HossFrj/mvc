﻿namespace Domain.Concrete.Schema.HR
{
    public class TransactionType_Category
    {
        public virtual int ID { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual byte TransactionTypeCode { get; set; }

        //public virtual Category Category { get; set; }
        //public virtual TransactionType TransactionType { get; set; }
    }
}
