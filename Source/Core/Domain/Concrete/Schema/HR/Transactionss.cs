﻿namespace Domain.Concrete.Schema.HR
{
    public class myTransaction
    {
        public myTransaction()
        {
            TransactionDateTime = DateTime.Now;
        }
        public virtual int ID { get; set; }
        public virtual int SourceFinancialAccountID { get; set; }
        public virtual int TransactionType_CategoryID { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Description { get; set; }

        public virtual DateTime TransactionDateTime { get; set; }

        
        //public virtual FinancialAccount SourceFinancialAccount { get; set; }
        //public virtual TransactionType_Category TransactionTypeCategory { get; set; }
    }
}
