﻿using Main.Models;
using Main.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Main.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountRepository _accountRepository;
        private readonly NHibernate.ISession _session;
        public AccountController(NHibernate.ISession session, AccountRepository accountRepository)
        {
            _session = session;
            _accountRepository = accountRepository;
        }

        public IActionResult Index()
        {
            var accounts = _accountRepository.Accounts.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return Json(accounts , jsonOptions);
        }

        [HttpGet("[controller]/validuseruame/{userName}")]
        public IActionResult ChcekUserName(string userName)
        {
            var accounts = _accountRepository.Accounts.ToList();
            List<Account> matchedGenders = new List<Account>();
            foreach (var Account in accounts)
            {
                var b = userName;
                var c = Account.UserName.ToLower();
                if (c == userName)
                {


                    return Json("valid");
                }

                else
                {
                    return Json("notValid");
                }


            }
            return Json(matchedGenders);

        }




        [HttpPost]
        public async Task<IActionResult> CreateAccount([FromBody] Account model)
        {
            if (ModelState.IsValid)
            {
                _accountRepository.BeginTransaction();
                try
                {
                    await _accountRepository.Save(model);
                    await _accountRepository.Commit();
                    return Ok(new { success = true, message = "Account created successfully." });
                }
                catch (Exception ex)
                {
                    await _accountRepository.Rollback();
                    return BadRequest(new { success = false, message = $"Failed to create account: {ex.Message}" });
                }
                finally
                {
                    _accountRepository.CloseTransaction();
                }
            }

            return BadRequest(new { success = false, message = "Invalid model state." });
        }
    }
}
