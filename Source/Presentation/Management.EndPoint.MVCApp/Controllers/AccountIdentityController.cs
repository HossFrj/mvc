﻿using Domain.Concrete.Schema.HR;
using Main.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;


namespace BootCampManagement.EndPoint.MVCApp.Controllers
{

    public class AccountIdentityController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        public IEnumerable<IdentityUser> Users { get; private set; } = Enumerable.Empty<IdentityUser>();
        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountIdentityController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;

        }

        [Authorize(Roles = "Admin")]
        public IActionResult UserList()
        {
            Users = _userManager.Users.ToList();
            return View(Users);
        }
        //-------------------------------------------------------------//
        public string ID { get; set; }

        [BindProperty]
        [Unicode]
        public string? UserName { get; set; }

        [BindProperty]
        public string? Password { get; set; }

        [BindProperty]
        [EmailAddress]
        [Unicode]
        public string? Email { get; set; }



        public async Task<IActionResult> CreateUser()
        {

            if (ModelState.IsValid)
            {
                //if (string.IsNullOrEmpty(Password))
                //{
                //    ModelState.AddModelError("Password", "Password is required.");
                //    return View();
                //}


                IdentityUser user = new()
                {
                    UserName = UserName,
                    Email = Email,
                };

                try
                {

                    IdentityResult result = await _userManager.CreateAsync(user, Password);


                    if (result.Succeeded)
                    {
                        return RedirectToAction("UserList");
                    }
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Password", "Password is required.");
                }
            }

            return View();
        }
        //-------------------------------------------------------------//
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UserEdit(string id)
        {
            async Task UserInfo()
            {
                var user = await _userManager.FindByIdAsync(id);
                UserName = user.UserName;
                Email = user.Email;

            }


            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(id);

                user.UserName = UserName;
                user.Email = Email;
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded && !string.IsNullOrEmpty(Password))
                {
                    await _userManager.RemovePasswordAsync(user);
                    result = await _userManager.AddPasswordAsync(user, Password);
                }
                if (result.Succeeded)
                {
                    return RedirectToAction("UserList");
                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }

            return View();
        }

        //-------------------------------------------------------------//
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("UserList");

        }



        //-------------------------------------------------------------//
        [BindProperty]
        public List<string> Roles { get; set; }
        [BindProperty]
        public IdentityUser CurentUser { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        public List<string> UserRoles { get; set; }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ManageUserRole(string id)
        {

            CurentUser = await _userManager.FindByIdAsync(id);

            //if (CurentUser == null)
            //{
            //    // Handle the case where the user is not found.
            //    return NotFound();
            //}

            UserRoles = (await _userManager.GetRolesAsync(CurentUser)).ToList();
            AllRoles = _roleManager.Roles.ToList();
            ViewBag.Roles = CurentUser;
            ViewBag.ID = id;
            ViewBag.AllRoles = AllRoles;
            var roleInfo = new 
            {
                Roles = UserRoles,
                ID = id,
                UserName = CurentUser,
                AllRole = AllRoles
            };

            return View(roleInfo);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PostUserRole(string id)
        {
            CurentUser = await _userManager.FindByIdAsync(id);
            AllRoles = _roleManager.Roles.ToList();

            foreach (var item in AllRoles)
            {
                if (Roles.Contains(item.Name))
                {
                    if (!(await _userManager.IsInRoleAsync(CurentUser, item.Name)))
                    {
                        await _userManager.AddToRoleAsync(CurentUser, item.Name);
                    }
                }
                else
                {
                    if (await _userManager.IsInRoleAsync(CurentUser, item.Name))
                    {
                        await _userManager.RemoveFromRoleAsync(CurentUser, item.Name);
                    }
                }
            }
            return RedirectToAction("UserList");

        }


    }





    
}



