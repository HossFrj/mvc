﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    public class AuthenticationIdentityController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;

        [BindProperty]
        public string UserName { get; set; }
        [BindProperty]
        public string Password { get; set; }
        [BindProperty]
        public string? ReturnUrl { get; set; }

        public AuthenticationIdentityController(SignInManager<IdentityUser> signInManager)
        {
            _signInManager = signInManager;
        }
        public IActionResult LogIN()
        {
            return View();
        }
        public async Task<IActionResult> PostLogIN()
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(UserName, Password, false, false);
                if (result.Succeeded)
                {
                    return Redirect(ReturnUrl ?? "/");
                }
                else
                {
                  ModelState.AddModelError("", "Invalid user name or password");
                }
            }
            return RedirectToAction("LogIN");
        }

        //-----------------------------------------------------------------------//

        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/");
        }

        //----------------------------------------------------------------------//

        public async Task<IActionResult> AccessDenied() { 
        
        return View();
        }


    }
}
