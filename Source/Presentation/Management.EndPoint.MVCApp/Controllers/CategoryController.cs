﻿using Application.Schema.HR;
using Domain.Concrete.Schema.HR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly NHibernate.ISession _session;
        public CategoryController(NHibernate.ISession session, CategoryRepository categoryRepository)
        {
            _session = session;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Index()
        {

            //var categories = _categoryRepository.Categories.ToList();
            //return Json(categories);
            var categories = _categoryRepository.Categories.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return Json(categories, jsonOptions);
        }
        public IActionResult CategoryList()
        {
            //var categories = _categoryRepository.Categories.ToList();
            //return Json(categories);
            var categories = _categoryRepository.Categories.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            var model = _session.Query<Category>().OrderBy(x => x.ParentID).ToList();

            return View(model);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _categoryRepository.BeginTransaction();

                var FA = await _categoryRepository.Categories.FirstOrDefaultAsync(x => x.ID == id);

                if (FA == null)
                    return BadRequest("چنین ایتمی وجود ندارد.");

                await _categoryRepository.Delete(FA);
                await _categoryRepository.Commit();

                return Json(new { success = true, message = " با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _categoryRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف : {ex.Message}" });
            }
            finally
            {
                _categoryRepository.CloseTransaction();
            }

        }
        private async Task SaveAsync(Category cat)
        {
            await _session.SaveOrUpdateAsync(cat);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Category cat)
        {


            try
            {
                await SaveAsync(cat);

                return Ok(new { Message = "اطلاعات با موفقیت ذخیره شد." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = $"خطا در ذخیره اطلاعات: {ex.Message}" });
            }
        }
        public IActionResult CreateCategory()
        {
            return View();
        }
    }
}
