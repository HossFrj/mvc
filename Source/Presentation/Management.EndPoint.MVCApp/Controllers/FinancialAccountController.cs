﻿using Application.Schema.HR;
using Domain.Concrete.Schema.HR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using System.Text.Json;
using Main.Repository;
using NHibernate.Linq;
using Main.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using Azure.Core;
using Microsoft.AspNetCore.Authorization;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    [Authorize]
    public class FinancialAccountController : Controller
    {
        private readonly FinancialAccountRepository _financialAccountRepository;
        private readonly NHibernate.ISession _session;

        public FinancialAccountController(NHibernate.ISession session, FinancialAccountRepository financialAccountRepository)
        {   
            _session = session;
            _financialAccountRepository = financialAccountRepository;
        }

        public IActionResult Index()
        {
            var financialAccounts = _financialAccountRepository.FinancialAccounts.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return Json(financialAccounts , jsonOptions);
        }
        public IActionResult FinancialAccountList()
        {
            var financialAccounts = _financialAccountRepository.FinancialAccounts.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return View(financialAccounts);
        }

     

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _financialAccountRepository.BeginTransaction();

                var FA = await _financialAccountRepository.FinancialAccounts.FirstOrDefaultAsync(x => x.ID == id);

                if (FA == null)
                    return BadRequest("چنین شخصی وجود ندارد.");

                await _financialAccountRepository.Delete(FA);
                await _financialAccountRepository.Commit();

                return Json(new { success = true, message = "شخص با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _financialAccountRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف شخص: {ex.Message}" });
            }
            finally
            {
                _financialAccountRepository.CloseTransaction();
            }
        }
        private async Task SaveAsync(FinancialAccount FA)
        {
            await _session.SaveOrUpdateAsync(FA);
        }
        [HttpPost]
        public async Task<IActionResult> CreateFA([FromBody] FinancialAccount FA)
        {

            
            try
            {
                await SaveAsync(FA);

                return Ok(new { Message = "اطلاعات با موفقیت ذخیره شد." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = $"خطا در ذخیره اطلاعات: {ex.Message}" });
            }
        }
        public IActionResult CreateFinancialAccount()
        {

            
            return View();
        }


    }
}
