﻿using Main.Models;
using Main.Repository;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;

namespace Main.Controllers
{
    public class GenderController : Controller

    {
        private readonly GenderReopsitory _GenderRep;

        private readonly NHibernate.ISession _session;

        public GenderController(NHibernate.ISession session)
        {
            _session = session;
            _GenderRep = new GenderReopsitory(_session);
        }


        public JsonResult Index()
        {
            var Genders = _GenderRep.Genders.ToList();

            return Json(Genders);
        }
        public IActionResult Privacy()
        {
            var genders = _GenderRep.Genders.ToList();
            return View(genders);
        }
        public IActionResult Serach()
        {
            var genders = _GenderRep.Genders.ToList();
            return View(genders);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(int id)
        {
            try
            {
                _GenderRep.BeginTransaction();

                var gender = await _GenderRep.Genders.FirstOrDefaultAsync(x => x.Code == id);

                if (gender == null)
                    return Json(BadRequest("چنین  وجود ندارد."));

                await _GenderRep.Delete(gender);
                await _GenderRep.Commit();

                return Json(new { success = true, message = " با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _GenderRep.Rollback();
                return Json(new { success = false, message = $"خطا در حذف : {ex.Message}" });
            }
            finally
            {
                _GenderRep.CloseTransaction();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Gender model)
        {
            if (ModelState.IsValid)
            {
                var gender = new Models.Gender
                {
                    Code = model.Code,
                    TitleEn = model.TitleEn,
                    TitleFa = model.TitleFa
                };

                _GenderRep.BeginTransaction();
                try
                {
                    await _GenderRep.Save(gender);
                    await _GenderRep.Commit();
                }
                catch
                {
                    await _GenderRep.Rollback();
                    return BadRequest("Failed to save the gender.");
                }
                finally
                {
                    _GenderRep.CloseTransaction();
                }
            }

            return BadRequest(ModelState);
        }


        /// <summary>
        /// متود برای دریافت جنست با کد
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("[controller]/GetByCode/{code}")]
        public async Task<JsonResult> GetByCode( byte code)
        {
            var Genders = _GenderRep.Genders.ToList();
            List<Gender> matchedGenders = new List<Gender>();
            foreach (var gender in Genders)
            {

                if (gender.Code == code)
                {
                    matchedGenders.Add(gender);
                }
            }

            return Json(matchedGenders);
        }

        /// <summary>
        /// متود برای دریافت بزرگترین کد
        /// </summary>
        /// <returns></returns>
        [HttpGet("[controller]/GetNextValue")]
        public async Task<JsonResult> GetNextValue()
        {
            var maxId = _GenderRep.Genders.Max(p => p.Code);
            return Json(maxId + 1);

        }
        /// <summary>
        /// بررسی معتبر بودن جندر ککد
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[controller]/checkVaild/{code}")]
        public async Task<JsonResult> checkVald(byte code)
        {

            var People = _GenderRep.Genders.ToList();
            List<Person> matchedGenders = new List<Person>();
            foreach (var gender in People)
            {

                if (gender.Code == code)
                {

                    if (gender.TitleEn != gender.TitleFa)
                    {
                        return Json("GenderCode valid");
                    }
                    else
                    {
                        return Json("GenderCode notValid");
                    }


                }

            }

            return Json(matchedGenders);
        }


    }
}
