﻿using Application.Schema.HR;
using Domain.Concrete.Schema.HR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    public class PasswordController : Controller
    {
        private readonly PasswordRepository _passwordRepository;
        private readonly NHibernate.ISession _session;
        public PasswordController(NHibernate.ISession session, PasswordRepository passwordRepository)
        {
            _session = session;
            _passwordRepository = passwordRepository;
        }

        public IActionResult Index()
        {
            var passwords = _passwordRepository.Passwords.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return Json(passwords, jsonOptions);

        }

        [HttpPost]
        public async Task<IActionResult> CreatePassword([FromBody] Password model)
        {
            if (ModelState.IsValid)
            {
                _passwordRepository.BeginTransaction();
                try
                {
                    await _passwordRepository.Save(model);
                    await _passwordRepository.Commit();
                    return Ok(new { success = true, message = "Password created successfully." });
                }
                catch (Exception ex)
                {
                    await _passwordRepository.Rollback();
                    return BadRequest(new { success = false, message = $"Failed to create password: {ex.Message}" });
                }
                finally
                {
                    _passwordRepository.CloseTransaction();
                }
            }

            return BadRequest(new { success = false, message = "Invalid model state." });
        }
    }
}
