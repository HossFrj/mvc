﻿using Main.Models;
using Main.Repository;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;

namespace Main.Controllers
{

    public class PersonController : Controller
    {
        private readonly NHibernate.ISession _session;
        private readonly PersonRepository _personRepository;
        public PersonController(NHibernate.ISession session, PersonRepository personRepository)
        {
            _session = session;
            _personRepository = personRepository;
        }
        public IActionResult Index()
        {
            var Persons = _personRepository.People.ToList();

            return Json(Persons);
        }


        [HttpPost]
        public async Task<IActionResult> CreatePerson([FromBody] Person model)
        {
            if (ModelState.IsValid)
            {
                var Pepele = new Models.Person
                {
                    BirthDate = model.BirthDate,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Gender = model.Gender,
                    //GenderCode = model.GenderCode,
                    NationalID = model.NationalID,
                    PhoneNumber = model.PhoneNumber

                };

                _personRepository.BeginTransaction();
                try
                {
                    await _personRepository.Save(Pepele);
                    await _personRepository.Commit();
                }
                catch
                {
                    await _personRepository.Rollback();
                    return BadRequest("Failed to save the gender.");
                }
                finally
                {
                    _personRepository.CloseTransaction();
                }
            }

            return BadRequest(ModelState);
        }


        /// <summary>
        /// متود برای دریافت بزرگترین آی دی
        /// </summary>
        /// <returns></returns>
        [HttpGet("[controller]/GetNextValue")]
        public async Task<JsonResult> GetNextValue()
        {
            var maxId = _personRepository.People.Max(p => p.ID);
            return Json(maxId + 2);

        }


        /// <summary>
        /// متود برای دریافت رکورد جدول با آی دی
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[controller]/GetById/{id}")]
        public async Task<JsonResult> GetByID(byte id)
        {
            //Console.WriteLine("Test Test Test " + GetNextValue);
            var People = _personRepository.People.ToList();
            List<Person> matchedGenders = new List<Person>();
            foreach (var person in People)
            {

                if (person.ID == id)
                {

                    matchedGenders.Add(person);


                }

            }

            return Json(matchedGenders);
        }
        /// <summary>
        /// بررسی معتبر بودن کد ملی
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[controller]/checkVaild/{id}")]
        public async Task<JsonResult> checkVald(byte id)
        {

            var People = _personRepository.People.ToList();
            List<Person> matchedGenders = new List<Person>();
            foreach (var person in People)
            {

                if (person.ID == id)
                {

                    if (person.NationalID.Length > 9)
                    {
                        return Json("NationalID valid");
                    }
                    else
                    {
                        return Json("NationalID notValid");
                    }


                }

            }

            return Json(matchedGenders);
        }

        /// <summary>
        /// بررسی اطلاعات دیتابیس با کوئری
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[controller]/checkVaild2/{id}")]
        public async Task<JsonResult> checkVaild2(byte id)
        {
            var person = _session.Query<Person>()
                        .Where(p => p.ID == id)
                        .FirstOrDefault();

            if (person != null)
            {
                if (person.NationalID.Length > 9)
                {
                    return Json("NationalID valid");
                }
                else
                {
                    return Json("NationalID notValid");
                }
            }
            else
            {
                return Json("NationalID NotFound");
            }
        }

        public IActionResult PersonCreate()
        {

            var maxId = _personRepository.People.Max(p => p.ID);
            ViewBag.MaxId = maxId + 2;
            return View();
        }

        public IActionResult SearchPerson()
        {
            return View();
        }
        public IActionResult ViewPerson()
        {
            //var People = _personRepository.People.ToList();

            var People = _personRepository.VeiwPersonWithRowNum();
            return View(People);
        }
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _personRepository.BeginTransaction();

                var person = await _personRepository.People.FirstOrDefaultAsync(x => x.ID == id);

                if (person == null)
                    return BadRequest("چنین شخصی وجود ندارد.");

                await _personRepository.Delete(person);
                await _personRepository.Commit();

                return Json(new { success = true, message = "شخص با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _personRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف شخص: {ex.Message}" });
            }
            finally
            {
                _personRepository.CloseTransaction();
            }
        }


    }
}
