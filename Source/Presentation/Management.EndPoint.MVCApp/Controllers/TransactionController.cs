﻿using Application.Schema.HR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using System.Text.Json;
using NHibernate.Linq;
using Domain.Concrete.Schema.HR;
using Microsoft.AspNetCore.Authorization;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly TransactionRepository _transactionRepository;
        private readonly NHibernate.ISession _session;
        public TransactionController(NHibernate.ISession session, TransactionRepository transactionRepository)
        {
            _session = session;
            _transactionRepository = transactionRepository;
        }

        public IActionResult Index()
        {
            //var transactions = _transactionRepository.Transactions.ToList();
            //return Json(transactions);
            var categories = _transactionRepository.Transactions.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };

            return Json(categories, jsonOptions);
        }
        public IActionResult TransactionList()
        {
            //var transactions = _transactionRepository.Transactions.ToList();
            //return Json(transactions);
            var categories = _transactionRepository.Transactions.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };

            return View(categories);
        }



        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _transactionRepository.BeginTransaction();
                var FA = await _transactionRepository.Transactions.FirstOrDefaultAsync(x => x.ID == id);
                if (FA == null)
                    return BadRequest("چنین شخصی وجود ندارد.");

                await _transactionRepository.Delete(FA);
                await _transactionRepository.Commit();
                return Json(new { success = true, message = "شخص با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _transactionRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف شخص: {ex.Message}" });
            }
            finally
            {
                _transactionRepository.CloseTransaction();
            }
        }
        private async Task SaveAsync(myTransaction Tra)
        {
            await _session.SaveOrUpdateAsync(Tra);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] myTransaction Tra)
        {


            try
            {
                await SaveAsync(Tra);

                return Ok(new { Message = "اطلاعات با موفقیت ذخیره شد." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = $"خطا در ذخیره اطلاعات: {ex.Message}" });
            }
        }
        public IActionResult CreateTransaction()
        {
            return View();
        }

    }
}

