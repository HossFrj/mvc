﻿using Application.Schema.HR;
using Domain.Concrete.Schema.HR;
using Main.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;


namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TransactionTypeController : Controller
    {
        private readonly TransactionTypeRepository _transactionTypeRepository;
        private readonly NHibernate.ISession _session;
        public TransactionTypeController(NHibernate.ISession session, TransactionTypeRepository transactionTypeRepository)
        {
            _transactionTypeRepository = transactionTypeRepository;
            _session = session;
        }

        public IActionResult Index()
        {
            var transactionTypes = _transactionTypeRepository.TransactionTypes.ToList();
            return Json(transactionTypes);
        }

        public IActionResult TransactionTypeList()
        {
            var transactionTypes = _transactionTypeRepository.TransactionTypes.ToList();
            return View(transactionTypes);
        }

        
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _transactionTypeRepository.BeginTransaction();

                var person = await _transactionTypeRepository.TransactionTypes.FirstOrDefaultAsync(x => x.Code == id);

                if (person == null)
                    return BadRequest("چنین چیزی وجود ندارد.");

                await _transactionTypeRepository.Delete(person);
                await _transactionTypeRepository.Commit();

                return Json(new { success = true, message = " با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _transactionTypeRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف : {ex.Message}" });
            }
            finally
            {
                _transactionTypeRepository.CloseTransaction();
            }
        }
        private async Task SaveAsync(TransactionType traType)
        {
            await _session.SaveOrUpdateAsync(traType);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TransactionType traType)
        {
            try
            {
                //await SaveAsync(traType);
                _transactionTypeRepository.BeginTransaction();
                await _transactionTypeRepository.Save(traType);
                await _transactionTypeRepository.Commit();

                return Ok(new { Message = "اطلاعات با موفقیت ذخیره شد." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = $"خطا در ذخیره اطلاعات: {ex.Message}" });
            }
        }
        public IActionResult CreateType()
        {
            return View();
        }
    }
}
