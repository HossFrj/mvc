﻿using Application.Schema.HR;
using Domain.Concrete.Schema.HR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using System.Text.Json;
using NHibernate.Linq;
using Microsoft.AspNetCore.Authorization;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TransactionType_CategoryController : Controller
    {
        private readonly TransactionTypeCategoryRepository _transactionTypeCategoryRepository;
        private readonly NHibernate.ISession _session;
        public TransactionType_CategoryController(NHibernate.ISession session, TransactionTypeCategoryRepository transactionTypeCategoryRepository)
        {
            _session = session;
            _transactionTypeCategoryRepository = transactionTypeCategoryRepository;
        }

        public IActionResult Index()
        {

            //var transactionTypeCategories = _transactionTypeCategoryRepository.TransactionTypeCategories.ToList();
            //return Json(transactionTypeCategories);
            var categories = _transactionTypeCategoryRepository.TransactionTypeCategories.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return Json(categories, jsonOptions);
        }
        public IActionResult TransactionType_CategoryList()
        {

            //var transactionTypeCategories = _transactionTypeCategoryRepository.TransactionTypeCategories.ToList();
            //return Json(transactionTypeCategories);
            var categories = _transactionTypeCategoryRepository.TransactionTypeCategories.ToList();
            var jsonOptions = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            return View(categories);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                _transactionTypeCategoryRepository.BeginTransaction();

                var FA = await _transactionTypeCategoryRepository.TransactionTypeCategories.FirstOrDefaultAsync(x => x.ID == id);

                if (FA == null)
                    return BadRequest("چنین چیزی وجود ندارد.");

                await _transactionTypeCategoryRepository.Delete(FA);
                await _transactionTypeCategoryRepository.Commit();

                return Json(new { success = true, message = " با موفقیت حذف شد." });
            }
            catch (Exception ex)
            {
                await _transactionTypeCategoryRepository.Rollback();
                return Json(new { success = false, message = $"خطا در حذف : {ex.Message}" });
            }
            finally
            {
                _transactionTypeCategoryRepository.CloseTransaction();
            }
        }
        private async Task SaveAsync(TransactionType_Category ttc)
        {
            await _session.SaveOrUpdateAsync(ttc);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TransactionType_Category ttc)
        {
            try
            {
                await SaveAsync(ttc);

                return Ok(new { Message = "اطلاعات با موفقیت ذخیره شد." });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = $"خطا در ذخیره اطلاعات: {ex.Message}" });
            }
        }
        public IActionResult CreateTransactionTypeCategory()
        {
            return View();
        }
    }
}
