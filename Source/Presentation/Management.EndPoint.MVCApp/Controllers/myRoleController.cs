﻿using Application.Schema.HR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BootCampManagement.EndPoint.MVCApp.Controllers
{
    public class myRoleController : Controller
    {
        private readonly RoleRepository _RoleRepository;
        private readonly NHibernate.ISession _session;

        public myRoleController(NHibernate.ISession session , RoleRepository ReRepository)
        {
            _session = session;
            _RoleRepository = ReRepository;
        }
       
        public IActionResult Index()
        {
            
            
           var roles = _RoleRepository.Roles.ToList();
            return Json(roles);
        }

        
    }
}
