﻿using Main.Repository;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;
using Application.Schema.HR;
using Microsoft.AspNetCore.Identity;
namespace Main.Infra;

public static class NHibernateExtensions
{
    public static IServiceCollection AddNHibernate(this IServiceCollection services, string connectionString)
    {

        var mapper = new ModelMapper();
        mapper.AddMappings(typeof(NHibernateExtensions).Assembly.ExportedTypes);
        HbmMapping domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

        var configuration = new Configuration();
        configuration.DataBaseIntegration(c =>
        {
            c.Dialect<NHibernate.Dialect.MsSql2012Dialect>();
            c.ConnectionString = connectionString;
            c.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
            c.LogFormattedSql = true;
            c.SchemaAction = SchemaAutoAction.Update;
            c.LogSqlInConsole = true;
        });
        configuration.AddMapping(domainMapping);

        var sessionFactory = configuration.BuildSessionFactory();


        services.AddSingleton(sessionFactory);
        services.AddScoped(factory => sessionFactory.OpenSession());


        services.AddScoped<GenderReopsitory>();
        services.AddScoped<PersonRepository>();
        services.AddScoped<AccountRepository>();
        services.AddScoped<PasswordRepository>();
        services.AddScoped<FinancialAccountRepository>();
        services.AddScoped<TransactionTypeRepository>();
        services.AddScoped<CategoryRepository>();
        services.AddScoped<TransactionTypeCategoryRepository>();
        services.AddScoped<TransactionRepository>();
        services.AddScoped<RoleRepository>();
        

        return services;
    }
}