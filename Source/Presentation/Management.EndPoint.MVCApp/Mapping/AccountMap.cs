﻿using Domain.Concrete.Schema.HR;
using Main.Models;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Main.Mapping
{
    public class AccountMap : ClassMapping<Account>
    {
        public AccountMap()
        {
            Schema("Account");
            Table("Account");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

            Property(x => x.PersonID, x =>
            {
                x.Type(NHibernateUtil.Int32);
                x.NotNullable(true);
                x.Column("[PersonID]");
            });
            Property(x => x.RoleID, x =>
            {
                x.Type(NHibernateUtil.Int32);
                x.NotNullable(true);
                x.Column("[RoleCode]");
            });


            Property(x => x.UserName, x =>
            {
                x.Length(50);
                x.NotNullable(true);
                x.Column("UserName");
            });

            Property(x => x.CreateDateTime, x =>
            {
                x.NotNullable(true);
                x.Column("CreateDateTime");
            });
             Property(x => x.PasswordID, x =>
            {
                x.NotNullable(true);
                x.Column("[PasswordID]");
            });

            ManyToOne(x => x.Person, m =>
            {
                m.Column("PersonID");
                m.Class(typeof(Person));
                m.NotNullable(true);
                m.ForeignKey("FK_Account_Person"); 
            });
            ManyToOne(x => x.Role, m =>
            {
                m.Column("RoleCode");
                m.Class(typeof(MyRole)); 
                m.NotNullable(true);
                m.ForeignKey("FK_Account_Role"); 
            });
            ManyToOne(x => x.Password, m =>
            {
                m.Column("PasswordID");
                m.Class(typeof(Password)); 
                m.NotNullable(true);
                m.ForeignKey("FK_Account_Password"); 
            });
        }
    }
}
