﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class CategoryMap : ClassMapping<Category>
    {
        public CategoryMap()
        {
            Schema("[Transaction]");
            Table("[Category]");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

            Property(x => x.ParentID, x =>
            {
                x.Type(NHibernateUtil.Int32);
                x.NotNullable(false);
                x.Column("ParentID");
            });
            Property(x => x.TitleEn, x =>
            {
                x.Length(100);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("TitleEn");
            });

            Property(x => x.TitleFa, x =>
            {
                x.Length(100);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("TitleFa");
            });

            Property(x => x.Description, x =>
            {
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("Description");
            });
            //ManyToOne(x => x.ParentCategory, m =>
            //{
            //    m.Column("[ParentID]");
            //    m.Class(typeof(Category));
            //    m.ForeignKey("FK_ChildCategory_ParentCategory");
            //});
            //ManyToOne(x => x.ParentCategory, m =>
            //{
            //    m.Column("ParentID");
            //    m.Class(typeof(Category));
            //    m.ForeignKey("FK_ChildCategory_ParentCategory");
            //});
        }
    }
}
