﻿

using Domain.Concrete.Schema.HR;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class FinancialAccountMap : ClassMapping<FinancialAccount>
    {
        public FinancialAccountMap()
        {
            Schema("Account");
            Table("FinancialAccount");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

            Property(x => x.AccountID, x =>
            {
                x.Column("[AccountID]");
                x.Type(NHibernateUtil.String);
                x.NotNullable(true); ;
            });

            Property(x => x.AccountNumber, x =>
            {
                x.Length(24);
                x.NotNullable(true);
            });

            Property(x => x.CardNumber, x =>
            {
                x.Length(16);
                x.NotNullable(true);
            });

            Property(x => x.CardExpireDate, x => x.NotNullable(true));
            Property(x => x.BankName, x => x.NotNullable(true));
            Property(x => x.Balance, x => x.NotNullable(true));
            //Property(x => x.Icon, x => x.Length(int.MaxValue));
            Property(x => x.Description, x => x.NotNullable(true));
            Property(x => x.CreateDateTime, x =>
            {
                x.NotNullable(true);
                x.Column(x => x.Default("GetDate()"));
            });

            //ManyToOne(x => x.Account, m =>
            //{
            //    m.Column("AccountID");
            //    //m.Class(typeof(IdentityUser)); // Adjust this to use IdentityUser
            //    m.NotNullable(true);
            //    m.ForeignKey("FK_FinancialAccount_AspNetUsers"); // Adjust to the correct foreign key constraint name
            //});
        }
    }
}
