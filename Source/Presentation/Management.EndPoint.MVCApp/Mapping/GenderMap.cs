﻿using Main.Models;
using NHibernate.Mapping.ByCode;
using NHibernate;
using NHibernate.Mapping.ByCode.Conformist;

namespace Main.Mapping
{
    public class GenderMap : ClassMapping<Gender>
    {
        public GenderMap()
        {
            Schema("Person");
            Table("Gender");

            Id(x => x.Code , x =>
            {  
                x.Generator(Generators.Assigned);
                x.Type(NHibernateUtil.Byte);
                x.Column("Code");
            });
            Property(b => b.TitleEn, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
            });
            Property(b => b.TitleFa, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
            });

          

        }


    }
}
