﻿using Domain.Concrete.Schema.HR;
using Main.Models;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class PasswordMap : ClassMapping<Password>
    {
        public PasswordMap()
        {
            Schema("Account");
            Table("Password");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });
            Property(x => x.AccountID, x =>
            {
                x.Type(NHibernateUtil.Int32);
                x.NotNullable(true);
                x.Column("[AccountID]");
            });

            Property(x => x.Pass, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("[Hash]");

            });

            Property(x => x.CreateDateTime, x =>
            {
                x.Type(NHibernateUtil.DateTime);
                x.NotNullable(true);
                x.Column("CreateDateTime");
            });

            ManyToOne(x => x.Account, m =>
            {
                m.Column("AccountID");
                m.Class(typeof(Account));
                m.NotNullable(true);
                m.ForeignKey("FK_Password_Account");
            });
        }
    }
}
