﻿using Main.Models;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Main.Mapping
{
    public class PersonMap : ClassMapping<Person>
    {
        public PersonMap()
        {
            Schema("Person");
            Table("Person");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

           

            Property(x => x.FirstName, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("FirstName");
            });

            Property(x => x.LastName, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("LastName");
            });

            Property(x => x.GenderCode, x =>
            {
                x.Type(NHibernateUtil.Byte);
                x.NotNullable(true);
                x.Column("[GenderCode]");
            });

            Property(x => x.NationalID, x =>
            {
                x.Length(11);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("NationalID");
            });

            Property(x => x.BirthDate, x =>
            {
                x.Type(NHibernateUtil.Date);
                x.NotNullable(true);
                x.Column("BirthDate");
            });

            Property(x => x.PhoneNumber, x =>
            {
                x.Length(20);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("PhoneNumber");
            });

            Property(x => x.Email, x =>
            {
                x.Length(100);
                x.Type(NHibernateUtil.String);
                x.Column("Email");
            });

            ManyToOne(x => x.Gender, m =>
            {
                m.Column("GenderCode");
                m.Class(typeof(Gender));

                m.ForeignKey("FK_Person_Gender");
            });


        }


    }
}
