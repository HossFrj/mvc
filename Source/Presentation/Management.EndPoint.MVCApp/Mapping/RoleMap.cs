﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class RoleMap : ClassMapping<MyRole>
    {
        public RoleMap()
        {
            Schema("Account");
            Table("Role");

            Id(x => x.ID, x =>
            {
               x.Generator(Generators.Assigned);
                x.Type(NHibernateUtil.Int32);
                x.Column("ID");
            });
            Property(b => b.RoleNameEn, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
            });
            Property(b => b.RoleNameFa, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
            });



        }
    }
}
