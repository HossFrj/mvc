﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class myTransactionMap : ClassMapping<myTransaction>
    {
        public myTransactionMap()
        {
            Schema("[Transaction]");

            Table("[Transaction]");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

            Property(x => x.SourceFinancialAccountID, x =>
            {
                x.NotNullable(true);
                x.Column("[SourceFinancialAccountID]");
            });

            Property(x => x.TransactionType_CategoryID, x =>
            {
                x.NotNullable(true);
                x.Column("[TransactionType_CategoryID]");
            });

           

            Property(x => x.Amount, x =>
            {
                x.NotNullable(true);
                x.Column("Amount");
            });

            Property(x => x.Description, x =>
            {
                x.Length(2000); 
                x.Type(NHibernateUtil.String);
                x.Column("Description");
            });
            Property(x => x.TransactionDateTime, x =>
            {
                x.NotNullable(true);
                x.Column(x => x.Default("GetDate()"));
            });
            //ManyToOne(x => x.SourceFinancialAccount, m =>
            //{
            //    m.Column("SourceFinancialAccountID");
            //    m.Class(typeof(FinancialAccount));
            //    m.NotNullable(false);
            //    m.ForeignKey("FK_Transaction_FinancialAccount");
            //});

            //ManyToOne(x => x.TransactionTypeCategory, m =>
            //{
            //    m.Column("TransactionType_CategoryID");
            //    m.Class(typeof(TransactionType_Category));
            //    m.NotNullable(false);
            //    m.ForeignKey("FK_Transaction_TransactionType_Category");
            //});

            //CheckConstraint("CHK_Amount", "[Amount] > 0");
        }
    }
}
