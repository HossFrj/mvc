﻿using Domain.Concrete.Schema.HR;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class TransactionTypeMap : ClassMapping<TransactionType>
    {
        public TransactionTypeMap()
        {
            Schema("[Transaction]");
            Table("[TransactionType]");

            Id(x => x.Code, x =>
            {
                x.Generator(Generators.Assigned);
                x.Type(NHibernateUtil.Byte);
                x.Column("Code");
            });
            Property(x => x.Code, x =>
            {
                x.Type(NHibernateUtil.Byte);
                x.Column("Code");
            });

            Property(x => x.TitleEn, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("TitleEn");
            });

            Property(x => x.TitleFa, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.String);
                x.NotNullable(true);
                x.Column("TitleFa");
            });
        }
    }
}
