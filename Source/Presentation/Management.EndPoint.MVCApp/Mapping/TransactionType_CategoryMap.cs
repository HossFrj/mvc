﻿using Domain.Concrete.Schema.HR;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace BootCampManagement.EndPoint.MVCApp.Mapping
{
    public class TransactionTypeCategoryMap : ClassMapping<TransactionType_Category>
    {
        public TransactionTypeCategoryMap()
        {
            Schema("[Transaction]");
            Table("[TransactionType_Category]");

            Id(x => x.ID, x =>
            {
                x.Generator(Generators.Identity);
                x.Column("ID");
            });

            Property(x => x.CategoryID, x =>
            {
                x.NotNullable(true);
                x.Column("[CategoryID]");
            });

            Property(x => x.TransactionTypeCode, x =>
            {
                x.NotNullable(true);
                x.Column("[TransactionTypeCode]");
            });

            //ManyToOne(x => x.Category, m =>
            //{
            //    m.Column("CategoryID");
            //    m.Class(typeof(Category));
            //    m.NotNullable(false);
            //    m.ForeignKey("FK_TransactionType_Category_Category");
            //});

            //ManyToOne(x => x.TransactionType, m =>
            //{
            //    m.Column("TransactionTypeCode");
            //    m.Class(typeof(TransactionType));
            //    m.NotNullable(false);
            //    m.ForeignKey("FK_TransactionType_Category_TransactionType");
            //});
        }
    }
}
