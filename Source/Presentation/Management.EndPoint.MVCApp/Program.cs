using Domain.Concrete.Schema.HR;
using Main.Infra;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NHibernate.AspNetCore.Identity;



var builder = WebApplication.CreateBuilder(args);
builder.Services.AddMvc();

var connection = "data source=.;initial catalog=PersonalAccountManagement;Integrated Security=True;TrustServerCertificate=True;";
//Console.WriteLine(connection);
builder.Services.AddNHibernate(connection);
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddDbContext<MyDbContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("BootCampManagement.EndPoint.MVCApp")));

builder.Services.AddIdentity<Microsoft.AspNetCore.Identity.IdentityUser, Microsoft.AspNetCore.Identity.IdentityRole>()
                .AddEntityFrameworkStores<MyDbContext>()
                .AddDefaultTokenProviders();

builder.Services.ConfigureApplicationCookie(options =>
{
    options.AccessDeniedPath = "/AuthenticationIdentity/AccessDenied";
    options.LoginPath = "/AuthenticationIdentity/LogIN";
   
});

builder.Services.AddControllersWithViews();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.MapDefaultControllerRoute();
app.UseAuthentication();
app.UseAuthorization();
app.UseDeveloperExceptionPage();
//app.UseStatusCodePagesWithRedirects("/AuthenticationIdentity/LogIN");

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();